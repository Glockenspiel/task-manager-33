package ru.t1.sukhorukova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.request.project.ProjectShowByIndexRequest;
import ru.t1.sukhorukova.tm.model.Project;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-show-by-index";

    @NotNull
    public static final String DESCRIPTION = "Display project by index.";

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");

        System.out.println("Enter project index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        @Nullable final ProjectShowByIndexRequest request = new ProjectShowByIndexRequest(getToken());
        request.setIndex(index);
        @Nullable final Project project = getProjectEndpoint().showByIndexProject(request).getProject();

        showProject(project);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
