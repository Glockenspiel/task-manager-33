package ru.t1.sukhorukova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.request.task.TaskUpdateByIndexRequest;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-update-by-index";

    @NotNull
    public static final String DESCRIPTION = "Update task by index.";

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY INDEX]");

        System.out.println("Enter task index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        System.out.println("Enter task name:");
        @Nullable final String name = TerminalUtil.nextLine();

        System.out.println("Enter task description:");
        @Nullable final String description = TerminalUtil.nextLine();

        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(getToken());
        request.setIndex(index);
        request.setName(name);
        request.setDescription(description);
        getTaskEndpoint().updateByIndexTask(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
