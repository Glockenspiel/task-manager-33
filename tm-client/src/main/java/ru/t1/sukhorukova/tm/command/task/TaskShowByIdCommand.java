package ru.t1.sukhorukova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.request.task.TaskShowByIdRequest;
import ru.t1.sukhorukova.tm.model.Task;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-show-by-id";

    @NotNull
    public static final String DESCRIPTION = "Display task by id.";

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");

        System.out.println("Enter task id:");
        @Nullable final String taskId = TerminalUtil.nextLine();

        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(getToken());
        request.setTaskId(taskId);
        @Nullable final Task task = getTaskEndpoint().showByIdTask(request).getTask();

        showTask(task);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
