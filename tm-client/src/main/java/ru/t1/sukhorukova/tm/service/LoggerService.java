package ru.t1.sukhorukova.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.api.service.ILoggerService;

import java.io.IOException;
import java.util.logging.*;

public final class LoggerService implements ILoggerService {

    @NotNull
    private final String configFile = "/logger.properties";

    @NotNull
    private final String commands = "COMMANDS";

    @NotNull
    private final String commandsFile = "./commands.xml";

    @NotNull
    private final String errors = "ERRORS";

    @NotNull
    private final String errorsFile = "./errors.xml";

    @NotNull
    private final String messages = "MESSAGES";

    @NotNull
    private final String messagesFile = "./messages.xml";

    @NotNull
    private final LogManager manager = LogManager.getLogManager();

    @NotNull
    private final Logger loggerRoot = Logger.getLogger("");

    @NotNull
    @Getter
    private final Logger loggerCommand = Logger.getLogger(commands);

    @NotNull
    @Getter
    private final Logger loggerError = Logger.getLogger(errors);

    @NotNull
    @Getter
    private final Logger loggerMessage = Logger.getLogger(messages);

    @NotNull
    private final ConsoleHandler consoleHandler = getConsoleHandler();

    private @NotNull ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    public LoggerService() {
        loadConfigFromFile();
        registry(loggerCommand, commandsFile, false);
        registry(loggerError, errorsFile, true);
        registry(loggerMessage, messagesFile, true);
    }

    private void loadConfigFromFile() {
        try {
            manager.readConfiguration(LoggerService.class.getResourceAsStream(configFile));
        } catch (@NotNull final IOException e) {
            loggerRoot.severe(e.getMessage());
        }
    }

    private void registry(
            @NotNull final Logger logger,
            @Nullable final String fileName,
            final boolean isConsole
    ) {
        try {
            if (isConsole) logger.addHandler(consoleHandler);
            logger.setUseParentHandlers(false);
            if (fileName != null && !fileName.isEmpty())
                logger.addHandler(new FileHandler(fileName));
        } catch (@NotNull final IOException e) {
            loggerRoot.severe(e.getMessage());
        }
    }

    @Override
    public void info(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        loggerMessage.info(message);
    }

    @Override
    public void debug(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        loggerMessage.fine(message);
    }

    @Override
    public void command(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        loggerCommand.fine(message);
    }

    @Override
    public void error(@Nullable final Exception e) {
        if (e == null) return;
        ;
        loggerError.log(Level.SEVERE, e.getMessage(), e);
    }

}
