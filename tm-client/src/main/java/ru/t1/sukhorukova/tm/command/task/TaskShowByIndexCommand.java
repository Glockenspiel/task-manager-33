package ru.t1.sukhorukova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.dto.request.task.TaskShowByIndexRequest;
import ru.t1.sukhorukova.tm.model.Task;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-show-by-index";

    @NotNull
    public static final String DESCRIPTION = "Display task by index.";

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");

        System.out.println("Enter task index:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        @NotNull final TaskShowByIndexRequest request = new TaskShowByIndexRequest(getToken());
        request.setIndex(index);
        @Nullable final Task task = getTaskEndpoint().showByIndexTask(request).getTask();

        showTask(task);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
