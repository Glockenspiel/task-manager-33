package ru.t1.sukhorukova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.api.endpoint.IUserEndpoint;
import ru.t1.sukhorukova.tm.api.service.ILocatorService;
import ru.t1.sukhorukova.tm.api.service.IUserService;
import ru.t1.sukhorukova.tm.dto.request.user.*;
import ru.t1.sukhorukova.tm.dto.response.user.*;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.model.Session;
import ru.t1.sukhorukova.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.sukhorukova.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final ILocatorService locatorService) {
        super(locatorService);
    }

    @NotNull
    @Override
    @WebMethod
    public UserChangePasswordResponse changePasswordUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserChangePasswordRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String password = request.getPassword();

        @Nullable final User user = getLocatorService().getUserService().setPassword(userId, password);

        return new UserChangePasswordResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLockResponse lockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();

        @Nullable final User user = getLocatorService().getUserService().lockOneByLogin(login);

        return new UserLockResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserRegistryResponse registryUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRegistryRequest request
    ) {
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();

        @Nullable final User user = getLocatorService().getUserService().create(login, password, email);

        return new UserRegistryResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserRemoveResponse removeUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRemoveRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();

        @Nullable final User user = getLocatorService().getUserService().removeOneByLogin(login);

        return new UserRemoveResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserUnlockResponse unlockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUnlockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();

        @Nullable final User user = getLocatorService().getUserService().unlockOneByLogin(login);

        return new UserUnlockResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserUpdateProfileResponse updateProfileUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUpdateProfileRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();

        @Nullable final User user = getLocatorService().getUserService().updateUser(userId, firstName, lastName, middleName);

        return new UserUpdateProfileResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserViewProfileResponse viewProfileUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserViewProfileRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();

        @Nullable final User user = getLocatorService().getUserService().findOneById(userId);

        return new UserViewProfileResponse(user);
    }

}
