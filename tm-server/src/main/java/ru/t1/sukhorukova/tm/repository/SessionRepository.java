package ru.t1.sukhorukova.tm.repository;

import ru.t1.sukhorukova.tm.api.repository.ISessionRepository;
import ru.t1.sukhorukova.tm.model.Session;

public final class SessionRepository extends AbstractUserOwnerRepository<Session> implements ISessionRepository {
}
