package ru.t1.sukhorukova.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.sukhorukova.tm.api.endpoint.*;
import ru.t1.sukhorukova.tm.api.repository.IProjectRepository;
import ru.t1.sukhorukova.tm.api.repository.ISessionRepository;
import ru.t1.sukhorukova.tm.api.repository.ITaskRepository;
import ru.t1.sukhorukova.tm.api.repository.IUserRepository;
import ru.t1.sukhorukova.tm.api.service.*;
import ru.t1.sukhorukova.tm.dto.request.system.*;
import ru.t1.sukhorukova.tm.dto.request.data.*;
import ru.t1.sukhorukova.tm.dto.request.project.*;
import ru.t1.sukhorukova.tm.dto.request.task.*;
import ru.t1.sukhorukova.tm.dto.request.user.*;
import ru.t1.sukhorukova.tm.endpoint.*;
import ru.t1.sukhorukova.tm.enumerated.Role;
import ru.t1.sukhorukova.tm.enumerated.Status;
import ru.t1.sukhorukova.tm.model.Project;
import ru.t1.sukhorukova.tm.model.Task;
import ru.t1.sukhorukova.tm.model.User;
import ru.t1.sukhorukova.tm.repository.ProjectRepository;
import ru.t1.sukhorukova.tm.repository.SessionRepository;
import ru.t1.sukhorukova.tm.repository.TaskRepository;
import ru.t1.sukhorukova.tm.repository.UserRepository;
import ru.t1.sukhorukova.tm.service.*;
import ru.t1.sukhorukova.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements ILocatorService {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(this);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService, sessionService);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    {
        registry(authEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(domainEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ':' + port + '/' + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    public void start(@NotNull final String[] args) {
        initPID();
        initLogger();
        initDemoData();
        initBackup();
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initDemoData() {
        @NotNull User admin = userService.create("admin", "admin", Role.ADMIN);
        @NotNull User user1 = userService.create("USER_01", "user01", "user01@address.ru");
        @NotNull User user2 = userService.create("USER_02", "user02", "user02@address.ru");
        @NotNull User user3 = userService.create("USER_03", "user03", "user03@address.ru");

        projectService.add(new Project(user1, "PROJECT_01", "Test project 1", Status.COMPLETED));
        projectService.add(new Project(user1, "PROJECT_18", "Test project 18", Status.IN_PROGRESS));
        projectService.add(new Project(user2, "PROJECT_02", "Test project 2", Status.NOT_STARTED));
        projectService.add(new Project(user3, "PROJECT_26", "Test project 26", Status.IN_PROGRESS));

        taskService.add(new Task(user1, "TASK_01", "Test task 1", Status.COMPLETED));
        taskService.add(new Task(user1, "TASK_18", "Test task 18", Status.IN_PROGRESS));
        taskService.add(new Task(user1, "TASK_02", "Test task 2", Status.NOT_STARTED));
        taskService.add(new Task(user2, "TASK_26", "Test task 26", Status.IN_PROGRESS));
    }

    private void initBackup() {
        backup.init();
    }

}
