package ru.t1.sukhorukova.tm.repository;

import ru.t1.sukhorukova.tm.api.repository.IProjectRepository;
import ru.t1.sukhorukova.tm.model.Project;

public final class ProjectRepository extends AbstractUserOwnerRepository<Project> implements IProjectRepository {
}
